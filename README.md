﻿# Simple Notes WebApp

Simple Notes WebApp is a front-end user interface for viewing, editing and removing notes stored online. This web application uses Angular, Bootstrap, Nodejs, ui-router, Angular Translate and Protractor.

### Installation
In the project directory execute this command to start the build process:
```
run_build.bat
```
[Nodejs](https://nodejs.org/) is required.

### How to Use Simple Notes WebApp
In the command line run server by this command from project's folder:
```
run_server.bat
```
Then visit this URL in your browser:
[http://localhost:9000/](http://localhost:9000/)


### Test
In the command line run this command from projet's folder to run tests:
```
run_tests.bat
```
