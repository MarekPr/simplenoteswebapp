﻿// set up
var express = require('express');
var app = express();
console.log ('__dirname: ' + __dirname);
// config
app.get('*', function (req, res) {
    console.log(req.url);
    if ('/' === req.url || '' === req.url) {
        res.sendFile('index.html', { root: __dirname });
    } else {
        res.sendFile(req.url, { root: __dirname });
    }
});
// listen
app.listen(9000);
console.log("App listening on port 9000");
