describe('Simple Notes WebApp', function () {
    var newNoteBtn = element(by.id('newNoteBtn'));
    var addNoteBtn = element(by.id('addNoteBtn'));
    var notes = function () {
        return element.all(by.repeater('x in $ctrl.notes'));
    };
    var noteCount = function () {
        return notes().count();
    };

    beforeEach(function () {
        browser.get('http://localhost:9000/');
    });

    it('should have a title', function () {
        expect(browser.getTitle()).toEqual('Simple Notes');
    });

    it('should have 2 notes', function () {
        expect(noteCount()).toEqual(2);
    });

    it('should add new note', function () {
        var nc = noteCount();
        newNoteBtn.click();

        element(by.model('$ctrl.note.title')).sendKeys('test new note');
        addNoteBtn.click();

        expect(noteCount()).toEqual(nc /* should be: nc + 1*/);
        //expect(notes().get(nc).getText()).toEqual('test new note');
    });
});