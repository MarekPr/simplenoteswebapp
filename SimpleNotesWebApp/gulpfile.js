var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano'),
    useref = require('gulp-useref'),
    gulpIf = require('gulp-if'),
    notify = require('gulp-notify'),
    del = require('del'),
    jshint = require('gulp-jshint');

gulp.task('jshint', function () {
    return gulp.src(['core.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(notify({ message: "task 'jshint' complete" }));
});

gulp.task('scripts', function () {
    return gulp.src(['index.html'])
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
        .pipe(notify({ message: "task 'scripts' complete" }));
});

gulp.task('components', function () {
    return gulp.src(['components/*.html'])
        .pipe(gulp.dest('dist/components'))
        .pipe(notify({ message: "task 'components' complete" }));
});

gulp.task('server', function () {
    return gulp.src(['server.js'])
        .pipe(gulp.dest('dist'))
        .pipe(notify({ message: "task 'server' complete" }));
});

gulp.task('clean', function () {
    return del(['dist/css', 'dist/js', 'dist/components', 'dist/index.html', 'dist/server.js']);
});

gulp.task('default', ['clean', 'jshint'], function () {
    gulp.start('scripts', 'components', 'server');
});