﻿$(document).ready(function () {
    $('.prevent-default').click(function (event) {
        event.preventDefault(); // cancel the event
    });
});

var simpleNodeNotes = angular.module('simpleNodeNotes', ['pascalprecht.translate', 'ui.router']);
simpleNodeNotes.constant('myConfig', {
    dataApiUrl: 'http://private-9aad-note10.apiary-mock.com'
}).service('myDataService', ['$http', 'myConfig', function ($http, myConfig) {
    return {
        getNotes: function () {
            console.log('-> getNotes()');
            return $http.get(myConfig.dataApiUrl + '/notes').then(function (response) {
                return Promise.resolve(response.data);
            }, function (error) {
                console.error('get /notes ...error');
                console.dir(error);
                return Promise.reject(error);
            });
        },

        getNote: function (idx) {
            return $http.get(myConfig.dataApiUrl + '/notes/' + idx).then(function (response) {
                return Promise.resolve(response.data);
            }, function (error) {
                console.error('get /notes/' + idx + ' ...error');
                console.dir(error);
                return Promise.reject(error);
            });
        },

        postNote: function (dataNote) {
            return $http.post(myConfig.dataApiUrl + '/notes', dataNote).then(function (response) {
                return Promise.resolve(response);
            }, function (error) {
                console.error('post /notes ...error');
                console.dir(error);
                return Promise.reject(error);
            });
        },

        putNote: function (idx, dataNote) {
            return $http.put(myConfig.dataApiUrl + '/notes/' + idx, dataNote).then(function (response) {
                return Promise.resolve(response);
            }, function (error) {
                console.error('put /notes/' + idx + ' ...error');
                console.dir(error);
                return Promise.reject(error);
            });
        },

        deleteNote: function (idx) {
            return $http.delete(myConfig.dataApiUrl + '/notes/' + idx).then(function (response) {
                return Promise.resolve(response);
            }, function (error) {
                console.error('delete /notes/' + idx + ' ...error');
                console.dir(error);
                return Promise.reject(error);
            });
        }
    };
}]).controller('mainController', ['$scope', 'myDataService', '$translate', function ($scope, myDataService, $translate) {
    console.log('----- mainController');

    this.changeLanguage = function (key) {
        console.log('changeLanguage: ' + key);
        $translate.use(key);
        return false;
    };

}]).component('helloComp', {
    templateUrl: 'components/helloComp.html',
    controller: ['$scope', function ($scope) {
    }],
    bindings: {}

}).component('aboutComp', {
    templateUrl: 'components/aboutComp.html',
    controller: ['$scope', function ($scope) {
    }],
    bindings: {}

}).component('notesComp', {
    templateUrl: 'components/notesComp.html',
    controller: ['$scope', '$state', function ($scope, $state) {
        this.newNote = function () {
            $state.go('note', { noteId: -1 });
        };
    }],
    bindings: {
        notes: '<'
    }

}).component('noteComp', {
    templateUrl: 'components/noteComp.html',
    controller: ['$scope', '$state', '$filter', 'myDataService', function ($scope, $state, $filter, myDataService) {
        this.cancelNote = function () {
            $state.go('notes');
        };
        this.updateNote = function () {
            console.log('update note ' + this.note.id + ', content: ' + this.note.title);
            myDataService.putNote(this.note.id, this.note).then(function (response) {
                $state.go('notes');
            });
        };
        this.addNote = function () {
            console.log('add note: ' + this.note.title);
            myDataService.postNote(this.note).then(function (response) {
                $state.go('notes');
            });
        };
        this.deleteNote = function () {
            if (confirm($filter('translate')('ConfirmDeleteTitle'))) {
                console.log('delete note: ' + this.note.id);
                myDataService.deleteNote(this.note.id).then(function (response) {
                    $state.go('notes');
                });
            }
        };
    }],
    bindings: {
        note: '<'
    }

}).config(['$translateProvider', '$stateProvider', '$urlRouterProvider', function ($translateProvider, $stateProvider, $urlRouterProvider) {
    // translations
    $translateProvider.translations('cs', {
        WelcomeTitle: 'Vítejte',
        AboutTitle: 'O nás',
        CzechLang: 'čeština',
        EnglishLang: 'angličtina',
        NotesTitle: 'Poznámky',
        IdTitle: 'Id',
        NoteTitle: 'Poznámka',
        ActionsTitle: 'Akce',
        EditAction: 'Upravit',
        UpdateTitle: 'Uložit',
        CancelTitle: 'Zrušit',
        AddTitle: 'Přidat',
        NewNoteTitle: 'Nová poznámka',
        DeleteTitle: 'Smazat',
        ConfirmDeleteTitle: 'Smazat danou poznámku?',
        NewNoteContent: 'Obsah nové poznámka.'
    });
    $translateProvider.translations('en', {
        WelcomeTitle: 'Welcome',
        AboutTitle: 'About',
        CzechLang: 'Czech',
        EnglishLang: 'English',
        NotesTitle: 'Notes',
        IdTitle: 'Id',
        NoteTitle: 'Note',
        ActionsTitle: 'Actions',
        EditAction: 'Edit',
        UpdateTitle: 'Save',
        CancelTitle: 'Cancel',
        AddTitle: 'Add',
        NewNoteTitle: 'New Note',
        DeleteTitle: 'Delete',
        ConfirmDeleteTitle: 'Are you sure to delete the note?',
        NewNoteContent: 'New note content.'
    });

    $translateProvider.preferredLanguage('cs');
    $translateProvider.fallbackLanguage(['cs', 'en']);

    $translateProvider.useSanitizeValueStrategy('escape');

    // routing
    $urlRouterProvider.otherwise('/notes');

    $stateProvider.state({
        name: 'hello',
        url: '/hello',
        views: {
            'commonView': 'helloComp'
        }
    });
    $stateProvider.state({
        name: 'about',
        url: '/about',
        views: {
            'commonView': 'aboutComp'
        }
    });
    $stateProvider.state({
        name: 'notes',
        url: '/notes',
        views: {
            'commonView': 'notesComp'
        },
        resolve: {
            notes: ['myDataService', function (myDataService) {
                return myDataService.getNotes();
            }]
        }
    });
    $stateProvider.state({
        name: 'note',
        url: '/notes/{noteId}',
        views: {
            'commonView': 'noteComp'
        },
        resolve: {
            note: ['$filter', 'myDataService', '$transition$', function ($filter, myDataService, $transition$) {
                var noteId = $transition$.params().noteId;
                if (noteId > 0) {
                    return myDataService.getNote(noteId);
                } else {
                    return Promise.resolve({ id: -1, title: $filter('translate')('NewNoteContent') });
                }
            }]
        }
    });

}]);
